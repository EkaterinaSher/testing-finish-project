import static io.restassured.RestAssured.baseURI;

import org.json.JSONObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.core.IsNull.notNullValue;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class GetPostsWithAuthTest {

    private void testGetPostsPages(String tokenf, int pageNum){
        given()
                .queryParam("page", pageNum)
                .queryParam("owner", "notMe")
                .header("X-Auth-Token", tokenf)
                .baseUri(baseURI)
                .expect()
                .body("items", notNullValue())
                .when()
                .get("/posts")
                .then()
                .statusCode(200);

    }

    @Test
    private void testGetPostsWithAuth() {

        RestAssured.baseURI = "https://test-stand.gb.ru/gateway/";

        Response loginResponse = given()
                .contentType("multipart/form-data")
                .multiPart("username", "1234567890111111111111")
                .multiPart("password", "3cd907ac6d")
                .when()
                .baseUri(baseURI)
                .post("/login");

        String sessionCookie = loginResponse.getCookie("session_id");

        var decodedResponse = URLDecoder.decode(sessionCookie, StandardCharsets.UTF_8);
        var json = new JSONObject(decodedResponse);
        var token = json.get("token");

        testGetPostsPages((String) token, 0);
        testGetPostsPages((String) token, 5);
        testGetPostsPages((String) token, -1);
        testGetPostsPages((String) token, 9999999);
        }
    }

